module.exports = function isValidBoardString(string) {
    // If string is missing or is longer than 9 characters, it's invalid:
    if (!string || string.length > 9) {
        return false;
    }

    // If string is not sequence of "x"s,
    // "o"s, and "+"s, it's invalid:
    if (!/^(x|o|\+|\s)*$/.test(string)) {
        return false;
    }

    const Xs = string.replace(/[^x]/g, ""),
        Os = string.replace(/[^o]/g, "");

    // If difference between number of Xs and Os is
    // too great, the string is invalid:
    if (Math.abs(Xs.length - Os.length) > 1) {
        return false;
    }

    return true;
};