const express = require("express"),
    Board = require("./model/board"),
    InvalidBoardStringError = require("./exception/invalid-board-string"),
    { Max } = require("./constants");

const app = express(),
    port = 3000;

app.get('/', (req, res) => {
    const boardString = req.query.board;
    let board;

    try {
        board = Board.fromString({ string: boardString, player: Max });
    } catch (InvalidBoardStringError) {
        res.sendStatus(400);
        return
    }

    res.status(200).send(board.getChildren()[0].toString());
});

app.listen(port, () => console.log(`Express server running on port ${port}`));
