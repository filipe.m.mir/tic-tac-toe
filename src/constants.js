const Player = require("./model/player");

const X = "x",
    O = "o";

module.exports = {
    X,
    O,
    Max: new Player({ value: X, multiplier: 1 }),
    Min: new Player({ value: X, multiplier: -1 })
}