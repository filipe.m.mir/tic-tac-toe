const isValidBoardString = require("../utils/is-valid-board-string"),
    InvalidBoardStringError = require("../exception/invalid-board-string"),
    { X, O, Max, Min } = require("../constants");

module.exports = class Board {
    static fromString({ string, player }) {
        if (!isValidBoardString(string)) {
            throw new InvalidBoardStringError(string);
        };

        const strArray = string.split(""),
            grid = [[null, null, null], [null, null, null], [null, null, null]];

        strArray.forEach((char, i) => {
            let value = null;

            switch (char) {
                case "x": value = X; break;
                case "o": value = O; break;
            }

            grid[Math.floor(i / 3)][i % 3] = value;
        })

        return new Board({ grid, player });
    }

    static fromBoard({ board, player }) {
        return new Board({ grid: JSON.parse(JSON.stringify(board._grid)), player });
    }

    constructor({ grid, player }) {
        this._grid = grid;
        this._player = player;
    }

    toString() {
        let str = "";

        this._grid.forEach((row) => {
            row.forEach((value) => {
                str += `${value === X || value === O ? value : " "}`;
            });
        });

        return str;
    }

    getChildren() {
        const avail = this._getEmptySquares(),
            children = [],
            opponent = this._player === Max ? Min : Max;

        avail.forEach(({ x, y }) => {
            const child = Board.fromBoard({ board: this, player: opponent });

            child._setSquare(opponent.getValue(), { x, y });

            children.push(child);
        })

        return children;
    }

    _getSquare({ x, y }) {
        return this._grid[x][y];
    }

    _setSquare(value, { x, y }) {
        if (this._getSquare({ x, y })) {
            throw new Error("Square already set");
        }

        this._grid[x][y] = value;
    }

    _getEmptySquares() {
        const empties = [];

        this._grid.forEach((row, x) => {
            row.forEach((cell, y) => {
                !cell && empties.push({ x, y });
            });
        });

        return empties;
    }
}