module.exports = class Player {
    constructor({ value, multiplier }) {
        this._value = value;
        this._multiplier = multiplier;
    }

    getValue() {
        return this._value;
    }

    getMultiplier() {
        return this.multiplier;
    }
};