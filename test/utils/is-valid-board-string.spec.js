const isValidBoardString = require("../../src/utils/is-valid-board-string");

test("isValidBoardString: returns true for a valid board string", function () {
    expect(isValidBoardString("x+++x+++o")).toBe(true);
});

test("isValidBoardString: returns true for another valid board string", function () {
    expect(isValidBoardString("x+o+xo++x")).toBe(true);
});

test("isValidBoardString: returns false if string is too long", function () {
    expect(isValidBoardString("xxxx++oo++++")).toBe(false);
});

test("isValidBoardString: returns false if there are too many Xs relative to Os", function () {
    expect(isValidBoardString("xxxx++oo+")).toBe(false);
});

test("isValidBoardString: returns false if there are too many Os relative to Xs", function () {
    expect(isValidBoardString("x+++++ooo")).toBe(false);
});

test("isValidBoardString: returns false if string has invalid characters", function () {
    expect(isValidBoardString("x+++t+++o")).toBe(false);
});

test("isValidBoardString: returns false if not a string", function () {
    expect(isValidBoardString(43)).toBe(false);
});

test("isValidBoardString: returns false if null", function () {
    expect(isValidBoardString(null)).toBe(false);
});

test("isValidBoardString: returns false if undefined", function () {
    expect(isValidBoardString(undefined)).toBe(false);
});