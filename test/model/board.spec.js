const Board = require("../../src/model/board"),
    InvalidBoardString = require("../../src/exception/invalid-board-string"),
    { X, O, Max, Min } = require("../../src/constants");

test("Board static fromString builder: Validly parses 'x+++x+++o'", function () {
    const expected = new Board({
        grid: [[X, null, null], [null, X, null], [null, null, O]],
        player: Max
    })
    expect(Board.fromString({ string: "x+++x+++o", player: Max })).toEqual(expected);
});

test("Board static fromString builder: Validly parses 'x+o+xo++x'", function () {
    const expected = new Board({
        grid: [[X, null, O], [null, X, O], [null, null, X]],
        player: Min
    })
    expect(Board.fromString({ string: "x+o+xo++x", player: Min })).toEqual(expected);
});

test("Board static fromString builder: throws on string that's invalid", function () {
    function throwInvalidStringIntoBuilder() {
        return Board.fromString({ string: "x+o+xo++xxx+", player: Max })
    }
    expect(throwInvalidStringIntoBuilder).toThrowError(InvalidBoardString);
});

test("Board.prototype._setSquare: sets the correct value", function () {
    const b = new Board({
        grid: [[X, null, null], [null, X, null], [null, null, O]],
        player: Max
    });

    b._setSquare(O, { x: 0, y: 1 });

    expect(b._getSquare({ x: 0, y: 1 })).toBe(O);
})

test("Board.prototype._setSquare: won't set a previously set square", function () {
    const b = new Board({
        grid: [[X, null, null], [null, X, null], [null, null, O]],
        player: Max
    });

    function setPreviouslySetSquare() {
        b._setSquare(O, { x: 0, y: 0 });
    }

    expect(setPreviouslySetSquare).toThrowError();
})

test("Board.prototype._getEmptySquares: returns all empty squares", function () {
    const b = new Board({
        grid: [[X, null, null], [null, X, null], [null, null, O]],
        player: Max
    });

    expect(b._getEmptySquares()).toEqual([
        { x: 0, y: 1 },
        { x: 0, y: 2 },
        { x: 1, y: 0 },
        { x: 1, y: 2 },
        { x: 2, y: 0 },
        { x: 2, y: 1 }
    ]);
})

test("Board.prototype._getEmptySquares: returns empty array if board is already full", function () {
    const b = new Board({
        grid: [[X, O, O], [O, X, X], [O, X, O]],
        player: Max
    });

    expect(b._getEmptySquares()).toEqual([]);
})

test("Board.prototype.getChildren: returns the correct children", function () {
    const b = new Board({
        grid: [[X, null, null], [X, O, null], [O, X, O]],
        player: Max
    }),
        c1 = new Board({
            grid: [[X, X, null], [X, O, null], [O, X, O]],
            player: Min
        }),
        c2 = new Board({
            grid: [[X, null, X], [X, O, null], [O, X, O]],
            player: Min
        }),
        c3 = new Board({
            grid: [[X, null, null], [X, O, X], [O, X, O]],
            player: Min
        });

    expect(b.getChildren()).toEqual(expect.arrayContaining([c1, c2, c3]));
})

test("Board.prototype.toString: returns correct string", function () {
    const b = new Board({
        grid: [[X, null, null], [null, X, null], [null, O, O]],
        player: Max
    });

    expect(b.toString()).toEqual("x   x  oo");
})




